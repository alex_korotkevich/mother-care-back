import { NextFunction, Request, Response } from "express";
import { ChildToken } from "../entity/child-token.entity";
import { ormClient } from "../orm-client";

const childTokenRepository = ormClient.getRepository(ChildToken);

export type TChildLogoutProps = Request & { childId: number };

export const authChildLogoutMiddleware = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  const token = req.headers.authorization as string;
  console.log(token);

  if (!token) {
    res.status(403).send("Unauthorized");
  }
  const tokenData: any = await childTokenRepository.findOne({
    where: { token },
  });
  console.log(tokenData);

  if (!tokenData) {
    res.status(403).send("Unauthorized");
  }
  (req as TChildLogoutProps).childId = tokenData.childId;
  next();
};

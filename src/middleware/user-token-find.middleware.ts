import { NextFunction, Request, Response } from "express";
import { UserToken } from "../entity/user-token.entity";
import { ormClient } from "../orm-client";

const userTokenRepository = ormClient.getRepository(UserToken);

export type TUserToken = Request & { id: number };

export const userTokenFindMiddleware = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  const token = req.headers.authorization as string;

  if (!token) {
    return res.status(403).send("Unauthorized");
  }
  const tokenData = await userTokenRepository.findOne({ where: { token } });

  if (!tokenData) {
    return res.status(403).send("Unauthorized");
  }
  (req as TUserToken).id = tokenData.userId;
  next();
};

import { NextFunction, Request, Response } from "express";

export const taskFindMiddleware = (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  const id = +req.params.id;
  if (!id) {
    return res.status(400).send("Bad Request");
  }
  next();
};

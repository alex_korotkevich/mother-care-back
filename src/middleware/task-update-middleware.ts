import { NextFunction, Request, Response } from "express";

export const taskUpdateMiddleware = (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  const id = req.params.id;
  if (!id) {
    return res.status(400).send("Bad Request");
  }
  const data = req.body;
  if (!data || !data.status) {
    return res.status(400).send("Bad Request");
  }
  next();
};

import { NextFunction, Request, Response } from "express";
import { UserToken } from "../entity/user-token.entity";
import { ormClient } from "../orm-client";

const userTokenRepository = ormClient.getRepository(UserToken);

export type TLogoutProps = Request & { userId: number };

export const authLogoutMiddleware = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  const token = req.headers.authorization as string;
  if (!token) {
    res.status(403).send("Unauthorized");
  }
  const tokenData: any = await userTokenRepository.findOne({
    where: { token },
  });
  if (!tokenData) {
    res.status(403).send("Unauthorized");
  }
  (req as TLogoutProps).userId = tokenData.userId;
  next();
};

import { NextFunction, Request, Response } from "express";
import { UserToken } from "../entity/user-token.entity";
import { ormClient } from "../orm-client";

const userTokenRepository = ormClient.getRepository(UserToken);

export type TChildToken = Request & { userId: number };

export const childTokenCreateMiddleware = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  const token = req.headers.authorization as string;
  if (!token) {
    return res.status(403).send("Unauthorized");
  }
  const tokenData = await userTokenRepository.findOne({ where: { token } });
  if (!tokenData) {
    return res.status(403).send("Unauthorized");
  }
  (req as TChildToken).userId = tokenData.userId;
  next();
};

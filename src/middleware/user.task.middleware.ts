import { NextFunction, Request, Response } from "express";
import { UserToken } from "../entity/user-token.entity";
import { ormClient } from "../orm-client";

const userTokenRepository = ormClient.getRepository(UserToken);

export type TUserTasksRequest = Request & { userId: number };

export const userTaskMiddleware = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  const id = +req.params.id;
  if (!id) {
    return res.status(400).send("Bad Request");
  }
  const token = req.headers.authorization;
  if (!token) {
    return res.status(400).send("Bad Request");
  }
  const tokenData = await userTokenRepository.findOne({
    where: { token },
  });
  if (!tokenData) {
    return res.status(400).send("Bad Request");
  }
  (req as TUserTasksRequest).userId = tokenData.userId;

  next();
};

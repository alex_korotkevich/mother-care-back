import { NextFunction, Request, Response } from "express";
import { ChildToken } from "../entity/child-token.entity";
import { ormClient } from "../orm-client";

const childTokenRepository = ormClient.getRepository(ChildToken);

export type TChildListToken = Request & { id: number };

export const childTokenMiddleware = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  const token = req.headers.authorization as string;
  if (!token) {
    return res.status(403).send("Unauthorized");
  }
  const tokenData = await childTokenRepository.findOne({
    where: { token },
  });
  if (!tokenData) {
    return res.status(403).send("Unauthorized");
  }
  (req as TChildListToken).id = tokenData.childId;
  next();
};

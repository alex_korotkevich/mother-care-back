import { NextFunction, Request, Response } from "express";

export const childRemoveMiddleware = (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  const id = req.params;
  if (!id) {
    return res.status(400).send("Bad Request");
  }
  next();
};

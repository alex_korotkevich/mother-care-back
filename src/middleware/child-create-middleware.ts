import { NextFunction, Request, Response } from "express";

export const childCreateMiddleware = (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  const data = req.body;
  if (!data || !data.login || !data.password || !data.name) {
    return res.status(400).send("Bad Request");
  }
  next();
};

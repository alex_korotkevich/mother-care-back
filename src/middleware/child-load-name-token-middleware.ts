import { NextFunction, Request, Response } from "express";
import { ChildToken } from "../entity/child-token.entity";
import { ormClient } from "../orm-client";

const childTokenRepository = ormClient.getRepository(ChildToken);

export type TChildLoadNameToken = Request & { id: number };

export const childLoadNameTokenMiddleware = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  const token = req.headers.authorization as string;
  if (!token) {
    return res.status(403).send("Unauthorized");
  }
  const tokenData: any = await childTokenRepository.findOne({
    where: { token },
  });
  if (!tokenData) {
    return res.status(403).send("Unauthorized");
  }
  (req as TChildLoadNameToken).id = tokenData.childId;
  next();
};

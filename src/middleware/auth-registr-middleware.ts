import { NextFunction, Request, Response } from "express";

export const authRegistrMiddleware = (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  const data = req.body;
  if (!data || !data.login || !data.password || !data.name) {
    res.status(400).send("Bad Request");
  }
  if (data.login.length < 6 || data.password.length < 6) {
    throw new Error("Password or login too short");
  }
  next();
};

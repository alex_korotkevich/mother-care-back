import { NextFunction, Request, Response } from "express";

export const taskCreateMiddleware = (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  const data = req.body;
  if (!data || !data.title || !data.text || !data.status) {
    return res.status(400).send("Bad Request");
  }
  next();
};

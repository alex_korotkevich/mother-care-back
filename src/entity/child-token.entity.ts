import { Column, Entity, PrimaryGeneratedColumn } from "typeorm";

@Entity()
export class ChildToken {
  @PrimaryGeneratedColumn("increment")
  id!: number;

  @Column()
  token!: string;

  @Column()
  childId!: number;
}

import { Column, Entity, PrimaryGeneratedColumn } from "typeorm";

@Entity()
export class UserToken {
  @PrimaryGeneratedColumn("increment")
  id!: number;

  @Column()
  token!: string;

  @Column()
  userId!: number;
}

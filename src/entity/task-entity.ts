import { Column, Entity, PrimaryGeneratedColumn } from "typeorm";

@Entity()
export class Task {
  @PrimaryGeneratedColumn("increment")
  id!: number;

  @Column()
  title!: string;

  @Column()
  text!: string;

  @Column()
  status!: string;

  @Column()
  userId!: number;

  @Column()
  childId!: number;
}

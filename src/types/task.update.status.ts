export type TTaskUpdateData = {
  status?: string;
  text?: string;
  title?: string;
};

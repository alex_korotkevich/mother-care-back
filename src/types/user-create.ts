export type TCreateUser = {
  name: string;
  login: string;
  password: string;
};

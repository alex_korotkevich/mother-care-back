export type TTaskCreateData = {
  title: string;
  text: string;
  status: string;
};

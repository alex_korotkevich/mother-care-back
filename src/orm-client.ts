import { DataSource } from "typeorm";
import { Childs } from "./entity/child-entity";
import { ChildToken } from "./entity/child-token.entity";
import { Task } from "./entity/task-entity";
import { Users } from "./entity/user-entity";
import { UserToken } from "./entity/user-token.entity";

export const ormClient = new DataSource({
  type: "postgres",
  username: process.env.DB_USER!,
  host: process.env.DB_HOST!,
  database: process.env.DB_DATABASE!,
  password: process.env.DB_PASSWORD!,
  port: +process.env.DB_PORT!,
  entities: [Task, Childs, Users, UserToken, ChildToken],
  synchronize: true,
});

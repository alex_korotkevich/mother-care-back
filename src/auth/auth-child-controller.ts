import { Request, Response, Router } from "express";
import {
  authChildLogoutMiddleware,
  TChildLogoutProps,
} from "../middleware/auth-child-logout-middleware";
import { authLoginMiddleware } from "../middleware/auth-login-middleware";
import { authChildService } from "./auth-child-service";
export const authChildRouter = Router();

authChildRouter.post(
  "/child/login",
  [authLoginMiddleware],
  async (req: Request, res: Response) => {
    try {
      const login = req.body.login as string;
      const password = req.body.password as string;
      const token = await authChildService.login(login, password);
      res.send(token);
    } catch (err) {
      res.status(403).send((err as Error).message);
    }
  }
);

authChildRouter.get(
  "/child/logout",
  [authChildLogoutMiddleware],
  async (req: Request, res: Response) => {
    try {
      const childId = (req as TChildLogoutProps).childId;
      await authChildService.logout(childId);
      res.end();
    } catch (err) {
      res.status(403).send((err as Error).message);
    }
  }
);

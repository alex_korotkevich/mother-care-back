import { Users } from "../entity/user-entity";
import { UserToken } from "../entity/user-token.entity";
import { ormClient } from "../orm-client";
import { TCreateUser } from "../types/user-create";
import { generateToken } from "../util/token";

const userTokenRepository = ormClient.getRepository(UserToken);
const userRepository = ormClient.getRepository(Users);

class AuthService {
  login = async (login: string, password: string) => {
    try {
      const user = await userRepository.findOne({ where: { login } });
      if (!user) {
        throw new Error("User not found");
      }
      if (user.password !== password) {
        throw new Error("Password doesn't match");
      }
      const token = generateToken();
      const userId = +user.id;
      await userTokenRepository.save({ userId, token });
      return token;
    } catch (err) {
      throw err;
    }
  };

  userRegistration = async (data: TCreateUser) => {
    try {
      await userRepository.save({
        name: data.name,
        login: data.login,
        password: data.password,
      });
    } catch (err) {
      throw err;
    }
  };

  logout = async (userId: number) => {
    try {
      await userTokenRepository.delete({ userId });
    } catch (err) {
      throw new Error("Data Base Error!!!");
    }
  };

  remove = async (userId: number) => {
    try {
      await userRepository.delete(userId);
      await userTokenRepository.delete({ userId });
    } catch (err) {
      throw err;
    }
  };
}

export const authService = new AuthService();

import { Request, Response, Router } from "express";
import { authLoginMiddleware } from "../middleware/auth-login-middleware";
import {
  authLogoutMiddleware,
  TLogoutProps,
} from "../middleware/auth-logout-middleware";
import { authRegistrMiddleware } from "../middleware/auth-registr-middleware";
import { TCreateUser } from "../types/user-create";
import { authService } from "./auth-user-service";

export const authRouter = Router();

authRouter.post(
  "/login",
  [authLoginMiddleware],
  async (req: Request, res: Response) => {
    try {
      const login = req.body.login as string;
      const password = req.body.password as string;
      const token = await authService.login(login, password);
      res.send(token);
    } catch (err) {
      res.status(403).send((err as Error).message);
    }
  }
);

authRouter.post(
  "/registration",
  [authRegistrMiddleware],
  async (req: Request, res: Response) => {
    try {
      const data = req.body as TCreateUser;
      await authService.userRegistration(data);
      res.end();
    } catch (err) {
      res.status(403).send((err as Error).message);
    }
  }
);

authRouter.get(
  "/logout",
  [authLogoutMiddleware],
  async (req: Request, res: Response) => {
    try {
      const userId = (req as TLogoutProps).userId;
      await authService.logout(userId);
      res.end();
    } catch (err) {
      res.status(403).send((err as Error).message);
    }
  }
);

authRouter.delete(
  "/",
  [authLogoutMiddleware],
  async (req: Request, res: Response) => {
    try {
      const userId = (req as TLogoutProps).userId;
      await authService.remove(userId);
      res.end();
    } catch (err) {
      res.status(403).send((err as Error).message);
    }
  }
);

import { Childs } from "../entity/child-entity";
import { ChildToken } from "../entity/child-token.entity";
import { ormClient } from "../orm-client";
import { generateToken } from "../util/token";

const childRepository = ormClient.getRepository(Childs);
const childTokenRepository = ormClient.getRepository(ChildToken);

class AuthChildService {
  login = async (login: string, password: string) => {
    try {
      const child = await childRepository.findOne({ where: { login } });
      if (!child) {
        throw new Error("User not found");
      }
      if (child.password !== password) {
        throw new Error("Password doesn't match");
      }
      const token = generateToken();
      const childId = +child.id;
      await childTokenRepository.save({ childId, token });
      return token;
    } catch (err) {
      throw err;
    }
  };

  logout = async (childId: number) => {
    try {
      await childTokenRepository.delete({ childId });
    } catch (err) {
      throw err;
    }
  };
}

export const authChildService = new AuthChildService();

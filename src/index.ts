require("dotenv").config();
import bodyParser from "body-parser";
import cors from "cors";
import express from "express";
import { authChildRouter } from "./auth/auth-child-controller";
import { authRouter } from "./auth/auth-user-controller";
import { childRouter } from "./child/child-controller";
import { ormClient } from "./orm-client";
import { taskRouter } from "./tasks/task.controller";
import { userTaskRouter } from "./tasks/user.task.controller";

const app = express();

app.use(cors());
app.use(bodyParser.json());
app.use([taskRouter, authRouter, userTaskRouter, childRouter, authChildRouter]);
ormClient.initialize();

app.listen(3200, () => {
  console.log("start server on 3200");
});

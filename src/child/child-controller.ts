import { Request, Response, Router } from "express";
import { childCreateMiddleware } from "../middleware/child-create-middleware";
import { childRemoveMiddleware } from "../middleware/child-remove-middleware";
import {
  childTokenCreateMiddleware,
  TChildToken,
} from "../middleware/child-token-create-middleware";
import {
  TUserToken,
  userTokenFindMiddleware,
} from "../middleware/user-token-find.middleware";
import { TCreateChild } from "../types/create.child";
import { childService } from "./child-service";

export const childRouter = Router();

childRouter.get(
  "/childs/me",
  [userTokenFindMiddleware],
  async (req: Request, res: Response) => {
    try {
      const id = (req as TUserToken).id;
      const user = await childService.findUser(id);
      res.send(user);
    } catch (err) {
      res.status(400).send((err as Error).message);
    }
  }
);

childRouter.post(
  "/childs",
  [childCreateMiddleware],
  [childTokenCreateMiddleware],
  async (req: Request, res: Response) => {
    try {
      const data = req.body as TCreateChild;
      const userId = (req as TChildToken).userId;
      const child = await childService.create(data, userId);
      res.send(child);
    } catch (err) {
      res.status(400).send((err as Error).message);
    }
  }
);

childRouter.get(
  "/childs",
  [childTokenCreateMiddleware],
  async (req: Request, res: Response) => {
    try {
      const userId = (req as TChildToken).userId;
      const childs = await childService.list(userId);
      res.send(childs);
    } catch (err) {
      res.status(400).send((err as Error).message);
    }
  }
);

childRouter.get(
  "/childs/:id",
  [childRemoveMiddleware],
  [childTokenCreateMiddleware],
  async (req: Request, res: Response) => {
    try {
      const id = +req.params.id;
      const userId = (req as TChildToken).userId;
      const user = await childService.findById(id, userId);
      res.send(user);
    } catch (err) {
      res.send((err as Error).message);
    }
  }
);

childRouter.delete(
  "/childs/:id",
  [childRemoveMiddleware],
  [childTokenCreateMiddleware],
  async (req: Request, res: Response) => {
    try {
      const id = +req.params.id;
      const userId = (req as TChildToken).userId;
      await childService.remove(id, userId);
      res.end();
    } catch (err) {
      res.status(400).send((err as Error).message);
    }
  }
);

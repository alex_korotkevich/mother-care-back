import { Childs } from "../entity/child-entity";
import { Users } from "../entity/user-entity";
import { ormClient } from "../orm-client";
import { TCreateChild } from "../types/create.child";

const childRepository = ormClient.getRepository(Childs);
const userRepository = ormClient.getRepository(Users);

class ChildService {
  create = async (data: TCreateChild, userId: number) => {
    try {
      const child = await childRepository.save({ userId, ...data });
      return child;
    } catch (err) {
      throw new Error("Data Base Error!!!");
    }
  };

  findById = async (id: number, userId: number) => {
    try {
      const user = await childRepository.findOne({ where: { id, userId } });
      return user;
    } catch (err) {
      throw new Error("Data Base Error!!!");
    }
  };

  list = async (userId: number) => {
    try {
      const childs = await childRepository.find({ where: { userId } });
      return childs;
    } catch (err) {
      throw new Error("Data Base Error!!!");
    }
  };

  findUser = async (id: number) => {
    try {
      const user = await userRepository.findOne({ where: { id } });
      return user;
    } catch (err) {
      throw new Error("Data Base Error!!!");
    }
  };

  findChildName = async (childId: number) => {
    try {
      const user = await childRepository.findOne({ where: { id: childId } });
      return user;
    } catch (err) {
      throw new Error("Data Base Error!!!");
    }
  };

  remove = async (id: number, userId: number) => {
    try {
      await childRepository.delete([id, userId]);
    } catch (err) {
      throw new Error("Data Base Error!!!");
    }
  };
}

export const childService = new ChildService();

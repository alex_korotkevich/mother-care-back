import { Request, Response, Router } from "express";
import {
  TUserTasksRequest,
  userTaskMiddleware,
} from "../middleware/user.task.middleware";
import { TTaskCreateData } from "../types/task.create.data";
import { userTaskService } from "./user.task.service";
export const userTaskRouter = Router();

userTaskRouter.get(
  "/user/tasks/:id",
  [userTaskMiddleware],
  async (req: Request, res: Response) => {
    try {
      const childId = +req.params.id;
      const userID = (req as TUserTasksRequest).userId;
      const tasks = await userTaskService.list(childId, userID);
      res.send(tasks);
    } catch (err) {
      res.status(400).send((err as Error).message);
    }
  }
);

userTaskRouter.post(
  "/user/tasks/:id",
  [userTaskMiddleware],
  async (req: Request, res: Response) => {
    try {
      const childId = +req.params.id;
      const userID = (req as TUserTasksRequest).userId;
      const data = req.body as TTaskCreateData;
      const task = await userTaskService.create(childId, userID, data);
      res.send(task);
    } catch (err) {
      res.status(400).send((err as Error).message);
    }
  }
);

userTaskRouter.delete(
  "/user/tasks/:id",
  [userTaskMiddleware],
  async (req: Request, res: Response) => {
    try {
      const taskId = +req.params.id;

      const userId = (req as TUserTasksRequest).userId;
      await userTaskService.remove(taskId, userId);
      res.end();
    } catch (err) {
      console.log(err);
      res.status(400).send((err as Error).message);
    }
  }
);

import { Task } from "../entity/task-entity";
import { ormClient } from "../orm-client";

const taskChildRepository = ormClient.getRepository(Task);

class TaskService {
  list = async (childId: number) => {
    try {
      const tasks = await taskChildRepository.find({ where: { childId } });
      return tasks;
    } catch (err) {
      throw new Error("Data Base Error!!!");
    }
  };

  find = async (id: number, childId: number) => {
    try {
      const task = await taskChildRepository.findOne({
        where: { id, childId },
      });
      return task;
    } catch (err) {
      throw new Error("Data Base Error!!!");
    }
  };

  changeStatus = async (id: number, status: string) => {
    try {
      await taskChildRepository.update(id, { status });
    } catch (err) {
      throw new Error("Data Base Error!!!");
    }
  };
}

export const taskService = new TaskService();

import { Task } from "../entity/task-entity";
import { ormClient } from "../orm-client";
import { TTaskCreateData } from "../types/task.create.data";

const taskUserRepository = ormClient.getRepository(Task);

class UserTaskService {
  list = async (childId: number, userId: number) => {
    try {
      const tasks = await taskUserRepository.find({
        where: { childId, userId },
      });
      return tasks;
    } catch (err) {
      throw new Error("Data Base Error!!!");
    }
  };

  create = async (childId: number, userId: number, data: TTaskCreateData) => {
    try {
      const task = await taskUserRepository.save({ childId, userId, ...data });
      return task;
    } catch (err) {
      throw new Error("Data Base Error!!!");
    }
  };

  remove = async (taskId: number, userId: number) => {
    try {
      await taskUserRepository.delete([taskId, userId]);
    } catch (err) {
      throw new Error("Data Base Error!!!");
    }
  };
}

export const userTaskService = new UserTaskService();

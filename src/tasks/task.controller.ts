import { Request, Response, Router } from "express";
import { childService } from "../child/child-service";
import {
  childLoadNameTokenMiddleware,
  TChildLoadNameToken,
} from "../middleware/child-load-name-token-middleware";
import {
  childTokenMiddleware,
  TChildListToken,
} from "../middleware/child-token-middleware";
import { taskFindMiddleware } from "../middleware/task-find-middleware";
import { taskUpdateMiddleware } from "../middleware/task-update-middleware";
import { taskService } from "./task.service";

export const taskRouter = Router();

taskRouter.get(
  "/child/tasks",
  [childTokenMiddleware],
  async (req: Request, res: Response) => {
    try {
      const childId = (req as TChildListToken).id;
      const tasks = await taskService.list(childId);
      res.send(tasks);
    } catch (err) {
      res.status(400).send((err as Error).message);
    }
  }
);

taskRouter.get(
  "/child/tasks/me",
  [childLoadNameTokenMiddleware],
  async (req: Request, res: Response) => {
    try {
      const id = (req as TChildLoadNameToken).id;
      const child = await childService.findChildName(id);
      res.send(child);
    } catch (err) {
      res.status(400).send((err as Error).message);
    }
  }
);

taskRouter.get(
  "/child/tasks/:id",
  [taskFindMiddleware],
  [childTokenMiddleware],
  async (req: Request, res: Response) => {
    try {
      const id = +req.params.id;
      const childId = (req as TChildListToken).id;
      const task = await taskService.find(id, childId);
      res.send(task);
    } catch (err) {
      res.status(400).send((err as Error).message);
    }
  }
);

taskRouter.put(
  "/child/tasks/:id",
  [taskUpdateMiddleware],
  async (req: Request, res: Response) => {
    try {
      const id = +req.params.id;
      const status = req.body.status as string;
      await taskService.changeStatus(id, status);
      res.end();
    } catch (err) {
      res.status(400).send((err as Error).message);
    }
  }
);
